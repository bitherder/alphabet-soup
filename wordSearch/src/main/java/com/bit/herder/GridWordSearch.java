package com.bit.herder;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class GridWordSearch {

	static int rowTotal, colTotal;

	// Setting vars for search direction.
	static int[] x = { -1, -1, -1, 0, 0, 1, 1, 1 };
	static int[] y = { -1, 0, 1, -1, 1, -1, 0, 1 };

	/**
	 * Search logic for searching the grid by direction.
	 * @param grid The grid to search through
	 * @param row The row for search
	 * @param column The column for search
	 * @param word The word we are searching for
	 * @return a string that defines the last coordinates
	 */
	static String searchGrid(char[][] grid, int row, int column, String word)
	{
		int lengthSearch = 8;
		// Examine first char of word
		if (grid[row][column] != word.charAt(0))
			return "";
        // Helper variables for return value
		int len = word.length();
		int lastRd = 0;
		int lastCd = 0;

		// Begin directional search
		for (int direction = 0; direction < lengthSearch; direction++) {
			// Set place to start
			int c, rowDirection = row + x[direction], columnDirection = column + y[direction];

			for (c = 1; c < len; c++) {
				if (rowDirection >= rowTotal || rowDirection < 0 || columnDirection >= colTotal || columnDirection < 0)
					break;

				if (grid[rowDirection][columnDirection] != word.charAt(c))
					break;

				// Copy last searched
				lastRd = rowDirection;
				lastCd = columnDirection;

				rowDirection += x[direction];
				columnDirection += y[direction];
			}

			// Return string of last coordination of last char
			if (c == len) {
				return lastRd + ":" + lastCd;
			}
		}
		return "";
	}

	/**
	 * Logic for searching for word in grid
	 * @param grid the grid of letters to search
	 * @param word the word to search for in the grid
	 */
	// Searches given word in a given
	// matrix in all 8 directions
	static void wordSearch(char[][] grid, String word) {
		for (int row = 0; row < rowTotal; row++) {
			for (int col = 0; col < colTotal; col++) {
				String foundLastCord = searchGrid(grid, row, col, word);
				if (grid[row][col]==word.charAt(0) && foundLastCord != "") {
					System.out.println(word + " " + row + ":" + col + " " + foundLastCord);
				}
			}
		}
	}


	public static void main(String[] args)throws IOException {
		List<String> fileContentList = new ArrayList<String>();

		// Read from passed in file argument as stated in readme
		Scanner sc = new Scanner(new FileReader(args[0])).useDelimiter("\n");
		String str;

		while (sc.hasNext()) {
			str = sc.next();
			fileContentList.add(str);
		}

		// Parse file and get grid size.
		String[] parsedLine = fileContentList.get(0).split("x");
		rowTotal = Integer.parseInt(parsedLine[0]);
		colTotal = Integer.parseInt(parsedLine[1]);

		// Create Grid
		char[][] gridBox = new char[rowTotal][colTotal];
		for (int x = 0; x < rowTotal; x++) {
			int readLine = x + 1;
            String[] fillGrid = fileContentList.get(readLine).split(" ");

            for (int y = 0; y < colTotal; y++) {
                gridBox[x][y] = fillGrid[y].charAt(0);
            }
        }
        // Parse out words to search for and loop through..
		List<String> wordList = fileContentList.subList(rowTotal + 1, fileContentList.size());

		// print each string in array
		for (String currentWord : wordList) {
			wordSearch(gridBox, currentWord);
		}

	}
}
